import { IAction } from "./action.type.ts";

export interface IStoreService {
  dispatch(action: IAction): void;
  select<T>(stateType: string, stateId?: number): T;
}

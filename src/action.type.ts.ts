export interface IAction {
  type: string;
  stateId: number;
}
